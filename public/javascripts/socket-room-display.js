function createRoomItem(room, isActive) {
  const li = document.createElement("li");
  li.classList.add("item-room", "p-2", "m-2");
  if (isActive) {
    li.classList.add("active");
  }
  li.setAttribute("data-index", room.index);
  li.innerHTML = `
      # ${room.title}
    `;
  li.addEventListener("click", (event) => {
    if (activeRoom._id !== room._id) {
      ioClient.emit("leaveRoom", activeRoom._id);
      const activeRoomItem = document.querySelector(".item-room.active");
      activeRoomItem.classList.remove("active");
      event.target.classList.add("active");
      activateRoom(room);
    }
  });
  return li;
}

function createEditableRoomItem({ room = { title: "" }, action }) {
  const li = document.createElement("li");
  const input = document.createElement("input");
  const validate = document.createElement("li");
  const cancel = document.createElement("li");

  li.classList.add("item-room", "p-2", "m-2");
  li.innerHTML = "# ";

  input.type = "text";
  input.value = room.title;
  input.name = room._id;

  validate.classList.add("fa", "fa-check", "text-success", "p-2", "m-2");
  validate.addEventListener("click", () => {
    const updatedRoom = { ...room, title: input.value };
    if (action === "create") {
      li.replaceWith(createRoomItem(updatedRoom));
      ioClient.emit("createRoom", updatedRoom);
    } else if (action === "edit") {
      li.replaceWith(createRoomItem(updatedRoom));
      ioClient.emit("updateRoom", updatedRoom);
    }
  });

  cancel.classList.add("fa", "fa-times", "text-danger");
  cancel.addEventListener("click", () => {
    if (action === "create") {
      li.remove();
    } else if (action === "edit") {
      li.replaceWith(createRoomItem(room));
    }
  });

  li.append(input, validate, cancel);

  return li;
}

function displayRooms() {
  const roomsContainer = document.querySelector(".list-rooms");
  items = rooms.map((room) =>
    createRoomItem(room, activeRoom._id === room._id)
  );
  roomsContainer.innerHTML = "";
  roomsContainer.prepend(...items);
}
